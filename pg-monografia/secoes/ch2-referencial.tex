\chapter{Referencial Teórico}
\label{ch:referencialTeorico}

Esse capítulo introduz os conceitos teóricos utilizados para implementação dos módulos propostos. O mesmo está separado em seções da seguinte forma:

\begin{itemize}
	\label{item:referencialTeorico}
	\item A Seção~\ref{sec:engenhariaSoftware} apresenta conceitos sobre Engenharia de Software que foram utilizados;
	\item A Seção~\ref{sec:desenvolvimentoWeb} apresenta os conceitos voltados ao desenvolvimento de software, apresentando os \textit{frameworks} utilizados e alguns conceitos sobre o desenvolvimento Web;
	\item A seção~\ref{sec:frameWeb} dá uma breve descrição do método FrameWeb~\cite{vitor-dissertacao2007}.
\end{itemize}

\section{Engenharia de Software}
\label{sec:engenhariaSoftware}

De forma a desenvolver um software com qualidade, criou-se um conjunto de práticas que permitiam aumentar a confiabilidade e a qualidade do software que estava sendo desenvolvido, denominada Engenharia de Software.
As práticas mais comuns utilizadas são definidas como (i) Levantamento de Requisitos, (ii) Análise/Especificação de Requisitos, (iii) Projeto, (iv) Implementação, (v) Testes e (vi) Implementação. Antes de introduzirmos tais práticas precisamos deixar claro o que é um requisito, portanto dispomos essa seção da seguinte maneira:

\begin{itemize}
	\item A Seção~\ref{subsec:requisitos} explica melhor o que é um requisito e suas definições;
 	\item A Seção~\ref{subsec:levantamentoRequisitos} explica os conceitos a prática de Levantamento de Requisitos;
 	\item A Seção~\ref{subsec:analiseEspecificacaoRequisitos} explica a prática de Análise e Especificação de Requisitos;
 	\item A Seção~\ref{subsec:projetoSoftware} explica a etapa de Projeto/Design de Software.
\end{itemize} 

\subsection{Requisitos}
\label{subsec:requisitos}
Nesta seção descrevemos de forma sucinta o que é um requisito e diferenciamos os tipos existentes.

Podemos dizer, com bases em diversas definições encontradas, que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento~\cite{Barcelos2018}.

Podemos definir os seguintes tipos de requisitos:

\begin{itemize}
\item \textbf{Requisitos Funcionais:} definem a funcionalidade desejada do software. São, na realidade, as funções que o cliente e os usuários querem que o software faça;

\item \textbf{Requisitos Não Funcionais:} são as qualidades gerais que um software deve possuir. Alguns exemplos:  usabilidade, desempenho, portabilidade, custos, dentre outros. De modo geral, esses requisitos são difíceis de validar;

\item \textbf{Requisitos de Domínio:} também são conhecidos como \textbf{Regras de Negócio}, são provenientes do domínio de aplicação do sistema e refletem características e restrições desse domínio~\cite{Barcelos2018}. São regras que garantem a implementação correta dos requisitos funcionais citados anteriormente.
\end{itemize}

Os tipos de requisitos acima apresentados devem ser passíveis de entendimentos por todos os envolvidos no processo de desenvolvimento de um software. Isso nos dá uma nova divisão de requisitos

\begin{itemize}
	\item \textbf{Requisitos de Cliente/Usuário:} são requisitos com um alto nível de abstração, de fácil entendimento para os usuários do sistema que não possuam um conhecimento técnico. Normalmente podem ser exemplificados com o uso de diagramas;
	
	\item \textbf{Requisitos de Sistema:} são requisitos com um alto nível de detalhamento técnico descrevendo as funções, serviços e restrições do sistema que serão utilizadas durante a implementação do sistema.
\end{itemize}

Como os requisitos de Sistema e de Cliente possuem propósitos diferentes é comum elaborar dois documentos de requisitos, sendo um deles denominado \textbf{Documento de Definição de Requisitos} e outro denominado \textbf{Documento de Especificação de Requisitos}.
No nosso projeto, unificamos esses dois documentos e o denominamos como
Documento de Requisitos. Para a criação desse documento realizamos as práticas de \textbf{Levantamento de Requisitos} e \textbf{Análise e Especificação de Requisitos}, representadas nas seções~\ref{subsec:levantamentoRequisitos} e~\ref{subsec:analiseEspecificacaoRequisitos}, respectivamente.

\subsection{Levantamento de Requisitos}
\label{subsec:levantamentoRequisitos}
É a fase inicial do processo de Especificação de Requisitos e em sua maioria consiste em conversas com os clientes --- também chamados de \textit{stakeholders} ---, consulta de documentos, estudos quanto ao negócio da organização para qual a aplicação está sendo desenvolvida, entre outras.
Esta etapa é importante para o processo pois facilita os próximos passos e miniminiza eventuais erros de especificação. A partir dela temos uma melhor definição dos requisitos funcionais, não-funcionais e as regras de negócio que o cliente espera da aplicação.
Os capítulos do Documento de Requisitos, nos apêndices, referentes à parte de levantamento de requisitos são a Introdução, Descrição de Propósito dos Módulos em conjunto com o Minimundo e os Requisitos do Usuário. Em tais capítulos foi utilizado a linguagem natural de forma que qualquer pessoa consiga identificar o que se espera do sistema.

\subsection{Análise e Especificação de Requisitos}
\label{subsec:analiseEspecificacaoRequisitos}
Nessa etapa é realizado o refinamento dos requisitos funcionais e, portanto, ocorre em paralelo com a etapa anterior de Levamento de Requisitos. Contudo, diferente da etapa de Levantamento onde temos a descrição dos requisitos com um baixo nível de detalhes, nesta etapa queremos um nível de detalhes mais alto permitindo que sejam criados modelos que auxiliem as próximas etapas de desenvolvimento.

A criação de um ou mais modelos que de acordo com \citeonline{Barcelos2018} são importantes pois tendem a facilitar comunicação entre membros da equipe, possibilitar a discussão de correções e modificações com o usuário, fomentar a documentação e possibilitar o estudo do comportamento do sistema.

Um modelo enfatiza um conjunto de características, que denominamos perspectivas. As mais importantes são:

\begin{itemize}
	\item Perspectiva estrutural: normalmente são utilizados os Diagramas de Classes para representar essa perspectiva, possuindo um foco no sobre o quê a aplicação se trata;
	
	\item Perspectiva comportamental: podem ser utilizados os Diagramas de Classe, Diagrama de Atividades, Diagrama de Estados e Diagramas de Interação para representar essa perspectiva. Esta perspectiva visa especificar quais funcionalidades/serviços que o sistema deve fornecer.
\end{itemize}

Podemos criar modelos mais ou menos detalhados, de acordo com as nossas necessidades e, os categorizando do maior para o menor nível de abstração, temos modelos conceituais, lógicos e físicos. Nessa etapa da especificação de requisitos normalmente temos a construção dos modelos conceituais. Nas fases seguintes os modelos lógicos e físicos serão construídos, de acordo com as tecnologias e ferramentas escolhidas para o sistema. Para o projeto em questão na perspectiva comportamental utilizamos os Modelos de Casos de Uso e para a perspectiva estrutural, utilizamos os Diagramas de Classes. 

Para auxiliar a criação desses modelos utilizamos uma Linguagem de Modelagem Unificada, também conhecida como UML. Tal linguagem auxilia de forma gráfica a especificação, visualização e documentação dos artefatos do sistema.

Com as etapas de Levantamento e Análise de Requisitos concluídas seguimos para a etapa de Projeto de Software, descrito na seção~\ref{subsec:projetoSoftware}


\subsection{Projeto de Software}
\label{subsec:projetoSoftware}
Esta etapa tem por objetivo vincular os modelos criados nas etapas anteriores a uma tecnologia específica. É nesta etapa que definimos qual linguagem de programação será utilizada, quais os mecanismos de persistências serão utilizados, se será utilizado algum padrão específico de projeto ou não, entre outras características mais específicas para o desenvolvimento dos requisitos do que a descrição dos mesmos. Normalmente os documentos produzidos nesta etapa, conforme \citeonline{Barcelos2018}, são:

\begin{itemize}
	\item Projeto da Arquitetura de Software: visa a definir os grandes componentes estruturais do software e seus relacionamentos;
	
	\item Projeto de Dados: tem por objetivo projetar a estrutura de armazenamento de dados necessária para implementar o software;
	
	\item Projeto de Interfaces: descreve como o software deverá se comunicar dentro dele mesmo (interfaces internas), com outros sistemas (interfaces externas) e com pessoas que o utilizam (interface com o usuário);
	
	\item Projeto Detalhado: tem por objetivo refinar e detalhar a descrição dos componentes estruturais da arquitetura do software.
\end{itemize}

No projeto em questão, unificamos todos esses documentos em um só dando o nome de Documento de Projeto de Sistema, que pode ser encontrados nos apêndices.

Os padrões utilizados para este sistema serão mais bem aprofundados nas seções de Desenvolvimento Web (\ref{sec:desenvolvimentoWeb}), e para auxiliar a criação de modelos com mais detalhes utilizaremos o Método FrameWeb, que é explicado em maiores detalhes na Seção~\ref{sec:frameWeb}.

\section{Desenvolvimento Web}
\label{sec:desenvolvimentoWeb}

Nessa seção iremos apresentar algumas das ferramentas e \textit{frameworks} utilizados para o desenvolvimento do trabalho. Durante a etapa de Especificação e Análise de Requisitos definimos qual o Paradigma de Programação, também chamado de Paradigma de Desenvolvimento, que usaremos para a elaboração do software. Na Tabela~\ref{tab:paradigma} citamos dois paradigmas, porém devemos ter consciência que outros existem e que dependendo do projeto podem se adequar melhor.

\begin{table}[h]
	\centering	
	\vspace{0.5cm}
	\caption{Paradigmas de Desenvolvimento}
	\label{tab:paradigma}
	\begin{tabular}{|p{2.5cm}|p{12cm}|}  \hline \rowcolor[rgb]{0.8,0.8,0.8}
		
	Paradigma & Descrição \\\hline
	
	Estruturado & Adota uma visão de desenvolvimento baseada em um modelo entrada-processamento-saída. No paradigma estruturado, os dados são considerados separadamente das funções que os transformam e a decomposição funcional é usada intensamente.\\\hline
	
	Orientado a Objetos & Esse paradigma parte do pressuposto que o mundo é povoado por objetos, ou seja, a abstração básica para se representar as coisas do mundo são os objetos.	\\\hline
	\end{tabular}	
\end{table}


O paradigma escolhido para a aplicação foi o Orientado a Objetos. A partir desse momento, podemos escolher algumas linguagens que seguem esse paradigma, por exemplo, Java, C++, entre outras. A escolha da linguagem de programação também é importante, pois de acordo com a linguagem temos funcionalidades e facilidades diferentes. Por ser a linguagem utilizada na construção inicial do sistema SIGME, continuamos com Java para o desenvolvimento dos módulos propostos.
Podemos usar a linguagem Java para criar diferentes tipos de aplicações. Porém, como estamos falando de uma aplicação para a Internet, utilizaremos uma plataforma padrão do Java para desenvolver aplicações voltadas para a Internet, ou para aplicações de grande porte. Essa plataforma é denominada Java EE (Java Plataform Enterprise Edition).

\subsection{Java Plataform Enterprise Edition - Java EE}
\label{subsec:JavaEE}
A Java EE é uma plataforma de desenvolvimento que possui diversas ferramentas para agilizar e auxiliar no desenvolvimento de aplicações Java. O foco dessa plataforma é prover aos desenvolvedores um conjunto poderoso de APIs para diminuir o tempo de desenvolvimento, reduzir a complexidade das aplicações e aumentar a performance das mesmas. O desenvolvedor utiliza de \textit{anotações} durante o desenvolvimento, diretamente nos arquivos Java para informar aos servidores Java EE as configurações dos componentes no momento execução da aplicação.

Para auxiliar o desenvolvimento, essa plataforma disponibiliza algumas tecnologias. Listamos as que são utilizadas no desenvolvimento do projeto:

\begin{itemize}	
	\item \textbf{JavaServer Faces (JSF)\footnote{https://docs.oracle.com/javaee/7/tutorial/jsf-intro.htm}:} JSF é uma tecnologia que nos permitir criar aplicações Java para Web utilizando componentes visuais pré-prontos, de forma que o desenvolvedor não se preocupe com JavaScript e HTML. Basta adicionarmos os componentes (calendários, tabelas, formulários) e eles serão renderizados e exibidos em formato HTML;
	
	\item \textbf{Java Persistence API (JPA)\footnote{https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm}:} é uma API padrão do Java para acessar, persistir e gerenciar dados entre os objetos e classes do Java com o modelo relacional do banco de dados. Foi definido como parte da especificação do EJB 3.0;
	
	\item \textbf{Enterprise Java Bean (EJB)\footnote{https://docs.oracle.com/javaee/7/tutorial/ejb-intro.htm}}: é um componente da plataforma Java EE que roda em um servidor de aplicação, tal como Glassfish, Wildfly, Tomcat, etc. Seu principal objetivo consiste em fornecer um desenvolvimento rápido e simplificado de aplicações Java, com base em componentes distribuídos, transacionais, seguros e portáveis;
	
	\item \textbf{CDI}\footnote{https://docs.oracle.com/javaee/7/tutorial/cdi-basic004.htm}: é um framework de injeção de dependências que foi incluído no Java EE 6. Com o CDI habilitado no projeto, todas as classes que atendam a determinadas regras podem ser chamadas de \textit{Managed Beans}.\footnote{https://docs.oracle.com/javaee/7/tutorial/cdi-basic004.htm} Classes que recebem tal característica são disponibilizadas para outras classes automaticamente pelo CDI, não sendo necessário criar instâncias de tais classes ou recebê-las como parâmetro.
\end{itemize}


\subsection{Model View Controller (MVC)}
\label{subsec:mvc}

Dentre as várias arquiteturas para se montar um sistema Web a mais utilizada é a denominada \textit{Model View Controller}, conhecida como MVC. Ela tem como benefício principal de isolar as regras de negócio da lógica da apresentação, ou lógica do usuário. Dessa forma, caso ocorra uma mudança na regra de negócio da aplicação a lógica da apresentação não será impactada.


Ela divide a aplicação em uma estrutura ternária, interconectada, de forma a separar cada funcionalidade, aumentando o reúso do código e permitindo o desenvolvimento paralelo da aplicação. Tais estruturas são conhecidas como:

\begin{enumerate}
	\item \textbf{Model}: é a camada que contém as entidades de domínio, de persistência do projeto bem como as regra de negócio da aplicação.
	Utilizamos na camada de modelo os \textit{frameworks} JPA, EJB e CDI já apresentados;
	
	\item \textbf{View}: é a parte visual para o usuário, ou seja, é a representação dos modelos definidos anteriormente. Nesse projeto, a visão é composta pelas páginas Web e recursos associados (imagens, folhas de estilo, scripts, etc.), gerenciados pelo JSF;
	
	\item \textbf{Controller}: é o link entre o usuário e o sistema, em outras palavras é quem faz a união entre a interface do usuário e as regras de negócio. Para o nosso projeto, o que faz o papel de controlador é o Faces Servlet, componente oferecido pelo JSF para este fim.
\end{enumerate}



\section{FrameWeb}
\label{sec:frameWeb}

%O FrameWeb (\textit{Framework-based Design Method for Web Engineering}) é um método de projeto para Engenharia Web focado no uso de \textit{frameworks}, baseado em metodologias e linguagens de modelagem bastante difundidas na área de Engenharia de Software sem, no entanto, impor nenhum processo de desenvolvimento específico. Em linhas gerais, o método assume que determinados \textit{frameworks} serão utilizados durante a construção da aplicação, define a arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema~\cite{vitor-dissertacao2007}.

O FrameWeb (\textit{Framework-based Design Method for Web Engineering}) é um método de projeto para construção de sistemas de informação baseado em \textit{frameworks}. Tal método assume que determinados tipos de \textit{frameworks} serão utilizados durante a construção da aplicação, definindo uma arquitetura básica e propondo modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}~\cite{vitor-dissertacao2007}.

A arquitetura proposta é baseada no padrão \textit{Service Layer} (Camada de Serviço)~\cite{fowler}, exibida na Figura~\ref{fig:camadaFrameWeb}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/camadaFrameWeb}
	\caption{Arquitetura Proposta Frame Web}
	\label{fig:camadaFrameWeb}
\end{figure}

Em tal arquitetura temos três camadas importantes.

\begin{itemize}
	\item \textbf{Camada de Apresentação (\textit{Presentation Tier}):} que apresenta as funcionalidades de interface com o usuário;
	\item \textbf{Camada de Negócio (\textit{Business Tier}):} contém as funcionalidades relativas às regras de negócio;
	\item \textbf{Camada de Persistência (\textit{Data Acces Tier}):} contém as funcionalidades de acesso ao banco de dados.
\end{itemize}

A linguagem \textbf{FrameWeb}, originalmente proposta por \citeonline{vitor-dissertacao2007}, propunha o uso de extensões leves da linguagem UML. Atualmente, a linguagem foi aprimorada para uma extensão completa do meta-modelo da UML, conforme proposto por \citeonline{martins-msc16}. Tal aprimoramento foi necessário para permitir que a linguagem FrameWeb pudesse ser independente de plataforma, permitindo o uso de diversas combinações de \textit{frameworks}, a escolha do modelador.

Os modelos definidos pelo método, utilizando a nomenclatura revisada por \citeonline{martins-msc16}, são apresentados a seguir:

\begin{enumerate}
	\item \textbf{Modelo de Entidades}
	
	É um diagrama de classes UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional. Os passos para a sua construção são:
	\begin{enumerate}
		\item  A partir do modelo de classes construído na fase de análise de requisitos, adequar o modelo à plataforma de implementação escolhida, indicando os tipos de dados de cada atributo, promovendo a classes atributos que devam ser promovidos, definindo a navegabilidade das associações, etc.; 
		\item Adicionar os mapeamentos de persistência.
	\end{enumerate}

	\item \textbf{Modelo de Persistência}
	
	Antes de definirmos o modelo de persistência definido pelo FrameWeb, temos que entender o que é a camada de persistência.
	A camada de persistência de uma aplicação provê as funcionalidades básicas para o armazenamento e a recuperação dos objetos do sistema. Para isolar a lógica de negócio e o banco de dados da aplicação utilizaremos no projeto o padrão de Objeto de Acesso a Dados, também conhecido como DAO (\textit{Data Access Object})~\cite{JavaPersistenceWithHibernate}.
	
	O padrão DAO define uma interface de operações de persistência, incluindo métodos para criar, recuperar, alterar, excluir e diversos tipos de consulta, relativa a uma particular entidade persistente, agrupando o código relacionado à persistência daquela entidade~\cite{JavaPersistenceWithHibernate}.
	
	Com as definições acima podemos dizer que o Modelo de Persistência é um diagrama de classes UML que representa as classes DAO existentes, que pertencem ao pacote de persistência da aplicação. Esse diagrama guia a construção das classes DAO apresentando para cada classe de domínio que necessita de acesso a dados, uma interface e uma classe concreta DAO que implementa a interface.
	
	Os passos para a construção desse modelo são:
	\begin{enumerate}
		\item Criar as interfaces e implementações concretas dos DAOs base;
		\item Definir quais classes de domínio precisam de lógica de acesso a dados e, portanto, precisam de um DAO;
		\item Para cada classe que precisa de um DAO, avaliar a necessidade de consultas específicas ao banco de dados, adicionando-as como operações nos respectivos DAOs.
	\end{enumerate}

	\item \textbf{Modelo de Navegação}
	
	É um diagrama de classe UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação como páginas Web, formulários HTML e classes de ação do \textit{framework Front Controller}.
	
	Os passos para a construção dos Modelos de Navegação são: 
	\begin{enumerate}
		\item Analisar os casos de uso modelados durante a Especificação de Requisitos, definir a granularidade das classes de ação e criá-las, definindo seus métodos. Utilizar, preferencialmente, nomes que as relacionem com os casos de uso ou cenários que representam;
		\item Identificar como os dados serão submetidos pelos clientes para criar as páginas, modelos e formulários, adicionando atributos à classe de ação;
		\item Identificar quais resultados são possíveis a partir dos dados de entrada, para criar as páginas e modelos de resultado, também adicionando atributos à classe de ação.
		\item Analisar se o modelo ficou muito complexo e considerar dividi-lo em dois ou mais diagramas. 
	\end{enumerate}

	
	\item \textbf{Modelo de Aplicação}
	
	É um diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências. Esse diagrama é utilizado para guiar a implementação das classes do pacote Aplicação e a configuração das dependências entre os pacotes Controle, Aplicação e Persistência, ou seja, quais classes de ação dependem de quais classes de serviço e quais DAOs são necessários para que as classes de serviço alcancem seus objetivos.
	
	Os passos para a construção do Modelo de Aplicação são: 
	
	\begin{enumerate}
		\item Analisar os casos de uso modelados durante a Especificação de Requisitos, definir a granularidade das classes de serviço e criá-las. Utilizar, preferencialmente, nomes que as relacionem com os casos de uso ou cenários que representam;
		\item  Adicionar às classes/interfaces os métodos que implementam a lógica de negócio, com atenção ao nome escolhido (preferencialmente relacionar o método ao cenário que implementa), aos parâmetros de entrada e ao retorno (observar a descrição do caso de uso);
		\item Por meio de uma leitura da descrição dos casos de uso, identificar quais DAOs são necessários para cada classe de aplicação e modelar as associações;
		\item Voltar ao modelo de navegação (se já foi construído), identificar quais classes de ação dependem de quais classes de serviço e modelar as associações.
	\end{enumerate}	
	
\end{enumerate}




